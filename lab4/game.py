import numpy as np
import itertools as it
import matplotlib.pyplot as plt

# Hide the flag game #
number_flags = 2
number_hiding_spots = 5
number_rounds = 500
maximum_score = number_flags * number_rounds
number_iterations = 50
epsilon = 0.2
eta = 1  # Exponential distribution parameter

# Find all possible flag placements
flag_positions = list(it.combinations(range(5), 2))

# Lying
B = 100  # 10, 50, 100


def game_loop(hiding_strategy, search_strategy, seeker_history, hider_history):
    scores = []
    fake_scores = []
    round_score = 0  # Real round score
    fake_score = 0
    remaining_B = B
    for current_round in range(number_rounds):

        # Hide the flags
        hiding_spots = hiding_strategy(hider_history, seeker_history, current_round + 1)

        # Try to find them
        search_attempts = search_strategy(seeker_history, current_round + 1)

        round_flags = 0
        for flag in search_attempts:
            if flag in hiding_spots:
                round_score += 1
                round_flags += 1
                fake_score += 1

        if round_flags == 0 and remaining_B > 0:
            # Lie and give the two points
            fake_score += 2
            remaining_B -= 1

        seeker_history[flag_positions.index(search_attempts)] += fake_score
        hider_history[flag_positions.index(hiding_spots)] -= round_score

        scores.append(round_score)
        fake_scores.append(fake_score)

    return scores, fake_scores


def base_search_strategy(history, round_number):
    exponential_noise = [np.random.exponential(scale=eta) for _ in history]
    noisy_arms = history / round_number + exponential_noise
    action = np.argmax(noisy_arms)
    return flag_positions[action]


# # # Hiding Strategies # # #
def uniform_random(history, search_history, round_number):
    # Parameters unused, there for consistency
    return flag_positions[np.random.choice(len(flag_positions))]


def epsilon_greedy(history, search_history, round_number):
    # Round number unused, there for consistency
    if np.random.uniform(low=0, high=1) < epsilon:
        return flag_positions[np.random.choice(len(flag_positions))]
    else:
        action_choice = np.argmin(search_history)
        return flag_positions[action_choice]


def fpl(history, search_history, round_number):
    exponential_noise = [np.random.exponential(scale=eta) for _ in history]
    noisy_arms = search_history / round_number + exponential_noise
    action = np.argmin(noisy_arms)
    return flag_positions[action]


# Initialise score and history trackers #
regrets_tracker = {'epsilon': []}
rewards_tracker = {'epsilon': []}
fake_rewards_tracker = {'epsilon': []}
fake_regrets_tracker = {'epsilon': []}

seeker_hist = {}
hider_hist = {}

# Define the functions and plot titles
# functions = {'uniform': uniform_random, 'epsilon': epsilon_greedy, 'fpl': fpl}

functions = {'epsilon': epsilon_greedy}
titles = {'epsilon': "Epsilon greedy hiding strategy with {} epsilon".format(epsilon)}

round_space = np.arange(1, 501)
max_round_scores = 2 * round_space

# Play the games #
for iteration in range(number_iterations):
    for key in regrets_tracker.keys():
        seeker_hist[key] = np.zeros(len(flag_positions))
        hider_hist[key] = np.zeros(len(flag_positions))

        real_scores, fakes = game_loop(functions[key], base_search_strategy, seeker_hist[key], hider_hist[key])
        regrets_tracker[key].append(max_round_scores - real_scores)
        rewards_tracker[key].append(real_scores)
        fake_rewards_tracker[key].append(fakes)
        fake_regrets_tracker[key].append(max_round_scores - fakes)


# Plot the results #
for key in rewards_tracker.keys():
    rewards_tracker[key] = np.array(rewards_tracker[key]) / round_space
    fake_rewards_tracker[key] = np.array(fake_rewards_tracker[key]) / round_space

    print("Reward for", key, "hiding strategy:", np.average(rewards_tracker[key]))
    plt.plot(range(number_rounds), np.average(rewards_tracker[key], 0), label='Real reward')
    plt.plot(range(number_rounds), np.average(fake_rewards_tracker[key], 0), label='Perceived reward')


plt.title("Average reward per round")
plt.xlabel("Round")
plt.ylabel("Avg Reward")
plt.legend()
plt.show()

for key in regrets_tracker.keys():
    regrets_tracker[key] = np.array(regrets_tracker[key]) / round_space
    fake_regrets_tracker[key] = np.array(fake_regrets_tracker[key]) / round_space

    print("Regret for", key, "hiding strategy:", np.average(regrets_tracker[key]))
    plt.plot(range(number_rounds), np.average(regrets_tracker[key], 0), label='Real regret')
    plt.plot(range(number_rounds), np.average(fake_regrets_tracker[key], 0), label='Perceived regret')

plt.title("Average regret per round")
plt.xlabel("Round")
plt.ylabel("Avg Regret")
plt.legend()
plt.show()
