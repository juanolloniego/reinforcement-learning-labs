import gym
import itertools
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D

env_name = "MountainCar-v0"
env = gym.make(env_name)
obs = env.reset()
n_states = 40
n_actions = 3
episodes = 1000

learning_rate = 1
min_lr = 0.005

gamma = 0.99
epsilon = 0.05
env = env.unwrapped
env.seed()
np.random.seed(0)


def discretization(env, obs):
    env_low = env.observation_space.low
    env_high = env.observation_space.high
    env_den = (env_high - env_low) / n_states
    pos_den = env_den[0]
    vel_den = env_den[1]
    pos_low = env_low[0]
    vel_low = env_low[1]
    pos_scaled = int((obs[0] - pos_low) / pos_den)
    vel_scaled = int((obs[1] - vel_low) / vel_den)
    return pos_scaled, vel_scaled


q_table = np.zeros((n_states, n_states, env.action_space.n))

for episode in range(episodes):
    obs = env.reset()
    total_reward = 0
    alpha = max(min_lr, learning_rate*(gamma**(episode//100)))
    steps = 0
    while True:
        # env.render()  # Comment for speed
        pos, vel = discretization(env, obs)

        # Choose the best known action
        a = np.argmax(q_table[pos][vel])
        # Exploration and exploitation
        if np.random.uniform(low=0, high=1) < epsilon:
            a = np.random.choice(env.action_space.n)

        obs, reward, terminate, _ = env.step(a)
        total_reward += abs(obs[0] + 0.5)
        pos_, vel_ = discretization(env, obs)

        q_table[pos][vel][a] = (1-alpha)*q_table[pos][vel][a] \
                               + alpha*(reward+gamma*np.max(q_table[pos_][vel_]))

        if terminate:
            break

max_J = 151
distances_from_truth = []
targets = q_table.reshape((n_states * n_states, n_actions))
poss = np.linspace(-1.2, 0.6, n_states)
vels = np.linspace(-0.07, 0.07, n_states)
permutations = np.array(list(itertools.product(poss, vels)))

for J in range(10, max_J):
    #J = 150
    # Solve with RBF
    kmeans = KMeans(n_clusters=J, random_state=0).fit(permutations)
    sig = np.std(permutations)

    # Construct train design matrix
    U = np.zeros((len(permutations), J))
    for i in range(len(permutations)):
        for j in range(J):
            U[i][j] = np.linalg.norm(permutations[i] - kmeans.cluster_centers_[j]) / sig

    # Solve for the weights
    w = np.dot((np.linalg.inv(np.dot(U.T, U))), U.T).dot(targets)
    preds = np.dot(U, w)

    dist = np.linalg.norm(preds - targets)
    distances_from_truth.append(dist)


plt.plot(range(10, max_J), distances_from_truth)
plt.title("Approximation accuracy for different number of basis functions")
plt.ylabel("Distance from true label")
plt.xlabel("Number of basis functions")
plt.show()


X, Y = np.meshgrid(poss, vels)

max_table = np.zeros((n_states, n_states))
reshaped_preds = preds.reshape((n_states, n_states, n_actions))
for i in range(n_states):
    for j in range(n_states):
        max_table[i][j] = -max(reshaped_preds[i][j])

# Plot the surface.
fig = plt.figure()
ax = fig.gca(projection='3d')
plt.title("Q table values")
plt.xlabel("Position")
plt.ylabel("Velocity")
surf = ax.plot_surface(X, Y, max_table, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
plt.show()

# Plot y against predictions
plt.plot(preds.reshape(4800), targets.reshape(4800), '.', color='green')
plt.show()

obs = env.reset()
while True:
    env.render()  # Comment for speed

    observation = np.array([obs[0], obs[1]])

    # Construct 'design matrix' for single point
    U = np.zeros(J)
    for j in range(J):
        U[j] = np.linalg.norm(observation - kmeans.cluster_centers_[j]) / sig

    predicted_reward = np.dot(U, w)
    a = np.argmax(predicted_reward)

    obs, reward, terminate, _ = env.step(a)
    if terminate:
        break
