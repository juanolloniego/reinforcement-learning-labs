import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.cluster import KMeans
from sklearn.model_selection import KFold


# Data from: https://archive.ics.uci.edu/ml/datasets/Airfoil+Self-Noise
rawData = np.genfromtxt('data/aerofoil_noise.data')
N, pp1 = rawData.shape

# Last column is target -> Noise produced
X = np.matrix(rawData[:, 0:pp1-1])
y = np.matrix(rawData[:, pp1-1]).T

# Normalise input data to be zero mean and have variance of 1
# Normalisation seems to fk things up
X = preprocessing.scale(X)


# ----------------- Part 2 ----------------- #

# Cost function is: MSE
def mse_cost(weights, data, labels):
    residuals = np.dot(data, weights) - labels
    return (1 / (2 * len(data))) * np.sum(np.square(residuals))


def mse_gradient(weights, data, labels):
    return (1 / len(data)) * (data.T * (np.dot(data, weights) - labels))


def weight_update_sgd(iterations, data, labels, initial_weights, learning_rate):
    weights = initial_weights
    costs = []

    for _ in range(iterations):
        # Randomise elements in X and y keeping order
        s = list(zip(data, labels))
        random.shuffle(s)
        data, labels = zip(*s)

        for idx in range(len(data)):
            cost = mse_cost(weights, data[idx], labels[idx])
            costs.append(cost)
            grad = mse_gradient(weights, data[idx].reshape((1, J)), labels[idx])
            weights = weights - learning_rate * grad

    return weights, costs


# ----------------- Part 1 ----------------- #

K_fold_validation_splits = 10
J = 75  # Number of radial basis functions (clusters) to use

lr_test_errors = []
rbf_test_errors = []
rbf_sgd_errors = []
plot_error = False

kf = KFold(n_splits=K_fold_validation_splits)
for train_index, test_index in kf.split(X):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # Solve LR with data provided
    w = np.dot((np.linalg.inv(np.dot(X_train.T, X_train))), X_train.T) * y_train

    lr_prediction = np.dot(X_test, w)
    lr_test_errors.append(np.sum(np.linalg.norm(y_test - lr_prediction)))

    # Solve with RBF
    kmeans = KMeans(n_clusters=J, random_state=0).fit(X_train)
    sig = np.std(X_train)

    # Construct train design matrix
    U = np.zeros((len(X_train), J))
    for i in range(len(X_train)):
        for j in range(J):
            U[i][j] = np.linalg.norm(X_train[i] - kmeans.cluster_centers_[j]) / sig

    # Solve RBF model, predict and plot
    w = np.dot((np.linalg.inv(np.dot(U.T, U))), U.T) * y_train

    # Construct test design matrix
    U = np.zeros((len(X_test), J))
    for i in range(len(X_test)):
        for j in range(J):
            U[i][j] = np.linalg.norm(X_test[i] - kmeans.cluster_centers_[j]) / sig

    rbf_prediction = np.dot(U, w)
    rbf_test_errors.append(np.sum(np.linalg.norm(y_test - rbf_prediction)))

    # ----------------- Part 2 ----------------- #

    n_iterations = 100

    # Construct train design matrix
    U = np.zeros((len(X_train), J))
    for i in range(len(X_train)):
        for j in range(J):
            U[i][j] = np.linalg.norm(X_train[i] - kmeans.cluster_centers_[j]) / sig

    # Part 2 - SGD RBF train error convergence
    w_0 = np.random.rand(J)
    final_w, cost_hist = weight_update_sgd(n_iterations, U, y_train, w_0, 0.0001)

    if plot_error:
        plt.title("Convergence of error")
        plt.plot(range(len(cost_hist)), cost_hist)
        plt.show()

    # Get the generalization error
    # Construct test design matrix
    U = np.zeros((len(X_test), J))
    for i in range(len(X_test)):
        for j in range(J):
            U[i][j] = np.linalg.norm(X_test[i] - kmeans.cluster_centers_[j]) / sig

    sgd_rbf_prediction = np.dot(U, final_w)
    rbf_sgd_errors.append(np.sum(np.linalg.norm(y_test - sgd_rbf_prediction)))


fig, ax = plt.subplots()
plt.title('Model Comparison')
plt.boxplot([rbf_test_errors, rbf_sgd_errors, lr_test_errors])
plt.ylabel("Distance from true label")
ax.set_xticklabels(['RBF Model', 'RBF SGD', 'Linear Model'])
plt.show()

# ----------------- Part 1 ----------------- #

# Solve the best approximation (using all data)
kmeans = KMeans(n_clusters=J, random_state=0).fit(X)
sig = np.std(X)

# Construct train design matrix
U = np.zeros((len(X), J))
for i in range(len(X)):
    for j in range(J):
        U[i][j] = np.linalg.norm(X[i] - kmeans.cluster_centers_[j]) / sig

# Solve RBF model, predict and plot
w = np.dot((np.linalg.inv(np.dot(U.T, U))), U.T) * y
rbf_approximation = np.dot(U, w)

plt.plot(rbf_approximation, y, '.')
plt.title("Best Approximation")
plt.ylabel("True label")
plt.xlabel("Model Approximation")
plt.show()

# ----------------- Part 2 ----------------- #
# RBF with SGD

n_iterations = 100

kmeans = KMeans(n_clusters=J, random_state=0).fit(X)
sig = np.std(X)

# Construct train design matrix
U = np.zeros((len(X), J))
for i in range(len(X)):
    for j in range(J):
        U[i][j] = np.linalg.norm(X[i] - kmeans.cluster_centers_[j]) / sig

w_0 = np.random.rand(J)
s1, s2 = weight_update_sgd(n_iterations, U, y, w_0, 0.0001)
plt.title("Convergence of error")
plt.xlabel("Steps")
plt.ylabel("Error")
plt.plot(range(len(s2)), s2)
plt.show()

# Plot the approximation found
rbf_approximation = np.dot(U, s1)

plt.plot(rbf_approximation, y, '.')
plt.title("SGD Approximation")
plt.ylabel("True label")
plt.xlabel("Model Approximation")
plt.show()
