import gym
import itertools
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D

env_name = "MountainCar-v0"
env = gym.make(env_name)
obs = env.reset()
n_states = 40
n_actions = 3
episodes = 100

learning_rate = 0.1
min_lr = 0.001

gamma = 0.99
epsilon = 0.05
env = env.unwrapped
env.seed()
np.random.seed(0)

# Number of radial functions
J = 50

poss = np.linspace(-1.2, 0.6, n_states)
vels = np.linspace(-0.07, 0.07, n_states)
permutations = np.array(list(itertools.product(poss, vels)))

kmeans = KMeans(n_clusters=J, random_state=0).fit(permutations)
sig = np.std(permutations)

weights = np.zeros((J, n_actions))
steps_per_episode = []

for episode in range(episodes):
    print("Episode: ", episode)
    steps = 0
    obs = env.reset()
    alpha = 0.4/8  # max(min_lr, learning_rate*(gamma**(episode//100)))
    while True:
        # env.render()  # Comment for speed

        # Choose the best known action
        U_old = np.zeros(J)
        for j in range(J):
            U_old[j] = np.exp(-1 * np.linalg.norm(obs - kmeans.cluster_centers_[j]) / sig)

        q_hat_sa = np.dot(U_old, weights)
        a = np.argmax(q_hat_sa)
        # Exploration and exploitation
        if np.random.uniform(low=0, high=1) < epsilon:
            a = np.random.choice(env.action_space.n)

        obs, reward, terminate, _ = env.step(a)
        steps += 1

        if terminate:
            weights[:, a] = weights[:, a] + alpha * (reward - q_hat_sa[a]) * U_old
            steps_per_episode.append(steps)
            steps = 0
            break

        # Choose A' as a function of q^(S', ., w)
        U = np.zeros(J)
        for j in range(J):
            U[j] = np.exp(-1 * np.linalg.norm(obs - kmeans.cluster_centers_[j]) / sig)

        q_hat_s_prime = np.dot(U, weights)
        a_prime = np.argmax(q_hat_s_prime)
        # Exploration and exploitation
        if np.random.uniform(low=0, high=1) < epsilon:
            a_prime = np.random.choice(env.action_space.n)

        weights[:, a] = weights[:, a] + alpha * (reward + gamma * q_hat_s_prime[a_prime] - q_hat_sa[a]) * U_old


plt.plot(range(episodes), steps_per_episode)
plt.title("Steps per episode")
plt.xlabel("Episode number")
plt.ylabel("Steps before goal")
plt.show()

# Plot the surface.
U = np.zeros((len(permutations), J))
for i in range(len(permutations)):
    for j in range(J):
        U[i][j] = np.exp(-1 * np.linalg.norm(permutations[i] - kmeans.cluster_centers_[j]) / sig)

predictions = np.dot(U, weights)

X, Y = np.meshgrid(poss, vels)
max_table = np.zeros((n_states, n_states))

reshaped_predictions = predictions.reshape((n_states, n_states, n_actions))
for i in range(n_states):
    for j in range(n_states):
        max_table[i][j] = -max(reshaped_predictions[i][j])

fig = plt.figure()
ax = fig.gca(projection='3d')
plt.title("Q table values")
plt.xlabel("Position")
plt.ylabel("Velocity")
surf = ax.plot_surface(X, Y, max_table, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
plt.show()
