import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

# Old Code

# Data from: https://archive.ics.uci.edu/ml/datasets/Airfoil+Self-Noise
rawData = np.genfromtxt('data/aerofoil_noise.data')
N, pp1 = rawData.shape

# Last column is target -> Noise produced
X = np.matrix(rawData[:, 0:pp1-1])
y = np.matrix(rawData[:, pp1-1]).T
print(X.shape, y.shape)

# Solve linear regression, plot target and prediction
w = np.dot((np.linalg.inv(np.dot(X.T, X))), X.T) * y
yh_lin = np.dot(X, w)
plt.plot(y, yh_lin, '.', color='magenta')

# J number of basis functions obtained by k-means clustering
# sigma set to standard deviation of entire data
J = 75

kmeans = KMeans(n_clusters=J, random_state=0).fit(X)
sig = np.std(X)

# Construct design matrix
U = np.zeros((N, J))
for i in range(N):
    for j in range(J):
        U[i][j] = np.linalg.norm(X[i] - kmeans.cluster_centers_[j]) / sig

# Solve RBF model, predict and plot
w = np.dot((np.linalg.inv(np.dot(U.T, U))), U.T) * y
yh_rbf = np.dot(U, w)
plt.plot(y, yh_rbf, '.', color='green')
print("Linear model distance between model and true label:", np.linalg.norm(y-yh_lin))
print("RBF model distance between model and true label:", np.linalg.norm(y-yh_rbf))
plt.show()