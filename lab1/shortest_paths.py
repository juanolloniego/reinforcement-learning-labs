import sys
import random
import numpy as np
import matplotlib.pyplot as plt


def all_pairs_shortest_path(g, maxsize=sys.maxsize):
    number_nodes = len(g.keys())
    dist = np.zeros((number_nodes, number_nodes))
    pred = np.zeros((number_nodes, number_nodes))
    for u in g:
        for v in g:
            dist[u][v] = maxsize
            pred[u][v] = None
            dist[u][u] = 0
            pred[u][u] = None
        for v in g[u]:
            dist[u][v] = g[u][v]
            pred[u][v] = u
    for mid in g:
        for u in g:
            for v in g:
                newlen = dist[u][mid] + dist[mid][v]
                if newlen < dist[u][v]:
                    dist[u][v] = newlen
                    pred[u][v] = pred[mid][v]

    return dist, pred


def construct_shortest_path(start, finish, pred):
    path = [finish]
    while finish != start:
        tmp = pred[start][finish]
        finish = tmp.astype(int)
        if finish is None:
            return None
        path.insert(0, finish)
    return path


def construct_graph(number_nodes, connection_density):
    result = {}

    for i in range(number_nodes):
        result[i] = {}
        for j in range(number_nodes):
            if i != j and random.random() <= connection_density:
                result[i][j] = 1

    return result


# Original Graph and code

# graph = {0: {1: 2, 4: 4},
#          1: {2: 3},
#          2: {3: 5, 4: 1},
#          3: {0: 8},
#          4: {3: 3}}
# print(graph)
# dist, pred = all_pairs_shortest_path(graph)
# print(dist)
# path03 = construct_shortest_path(0, 3, pred)
# print(path03)

# 100 nodes with 30% connection prob.
dens = 0.3
graph = construct_graph(100, dens)

_, pred = all_pairs_shortest_path(graph)

list_distances = []
for i in graph:
    for j in graph:
        path = construct_shortest_path(i, j, pred)
        list_distances.append(len(path) - 1)

plt.hist(list_distances, [0, 1, 2, 3, 4])
plt.xticks([0, 1, 2, 3, 4])
plt.title("Connection density of {}".format(dens))
plt.xlabel("Length")
plt.ylabel("Frequency")
plt.show()


# Decrease the number of connections and plot results
dens = 0.1
graph = construct_graph(100, dens)
_, pred = all_pairs_shortest_path(graph)

list_distances = []
for i in graph:
    for j in graph:
        path = construct_shortest_path(i, j, pred)
        list_distances.append(len(path) - 1)

plt.hist(list_distances, [0, 1, 2, 3, 4])
plt.xticks([0, 1, 2, 3, 4])
plt.title("Connection density of {}".format(dens))
plt.xlabel("Length")
plt.ylabel("Frequency")
plt.show()


# Drastically increase it
dens = 0.8
graph = construct_graph(100, dens)
_, pred = all_pairs_shortest_path(graph)

list_distances = []
for i in graph:
    for j in graph:
        path = construct_shortest_path(i, j, pred)
        list_distances.append(len(path) - 1)

plt.hist(list_distances, [0, 1, 2, 3, 4])
plt.xticks([0, 1, 2, 3, 4])
plt.title("Connection density of {}".format(dens))
plt.xlabel("Length")
plt.ylabel("Frequency")
plt.show()
