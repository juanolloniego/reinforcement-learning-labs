import gym
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

env_name = "MountainCar-v0"
env = gym.make(env_name)
obs = env.reset()
n_states = 40
episodes = 1000

learning_rate = 1
min_lr = 0.005

gamma = 0.99
epsilon = 0.05
env = env.unwrapped
env.seed()
np.random.seed(0)


def discretization(env, obs):
    env_low = env.observation_space.low
    env_high = env.observation_space.high
    env_den = (env_high - env_low) / n_states
    pos_den = env_den[0]
    vel_den = env_den[1]
    pos_low = env_low[0]
    vel_low = env_low[1]
    pos_scaled = int((obs[0] - pos_low) / pos_den)
    vel_scaled = int((obs[1] - vel_low) / vel_den)
    return pos_scaled, vel_scaled


q_table = np.zeros((n_states, n_states, env.action_space.n))
total_steps = 0

steps_per_episode = []
positions_to_track = [(4, 20, 1), (15, 17, 1), (25, 25, 1), (32, 35, 1)]
q_table_tracked_values = [[] for pos in positions_to_track]


for episode in range(episodes):
    print("Episode:", episode)
    obs = env.reset()
    total_reward = 0
    alpha = max(min_lr, learning_rate*(gamma**(episode//100)))
    steps = 0
    while True:
        # env.render()  # Comment for speed
        pos, vel = discretization(env, obs)

        # Exploration and exploitation
        if np.random.uniform(low=0, high=1) < epsilon:
            # Randomly choose and action
            a = np.random.choice(env.action_space.n)
        else:
            # Choose the best known action
            a = np.argmax(q_table[pos][vel])

        obs, reward, terminate, _ = env.step(a)
        total_reward += abs(obs[0] + 0.5)
        pos_, vel_ = discretization(env, obs)

        q_table[pos][vel][a] = (1-alpha)*q_table[pos][vel][a] \
                               + alpha*(reward+gamma*np.max(q_table[pos_][vel_]))

        for idx, (position, velocity, action) in enumerate(positions_to_track):
            q_table_tracked_values[idx].append(q_table[position][velocity][action])

        steps += 1
        total_steps += 1
        if terminate:
            break

    steps_per_episode.append(steps)


plt.plot(range(episodes), steps_per_episode)
plt.xlabel("Episode")
plt.ylabel("Steps")
plt.title("Steps with {} as initial learning rate".format(learning_rate))
plt.show()

plt.title("Q_table values")
for idx, (pos, vel, a) in enumerate(positions_to_track):
    plt.plot(range(total_steps), q_table_tracked_values[idx], label="pos {}, vel {}".format(pos, vel))

plt.ylabel("Value")
plt.xlabel("Steps")
plt.legend()
plt.show()


# Plot the q-table max values for each position and velocity pair
# Make data.
X = np.arange(0, n_states, 1)
Y = np.arange(0, n_states, 1)
X, Y = np.meshgrid(X, Y)

max_table = np.zeros((n_states, n_states))
for i in range(n_states):
    for j in range(n_states):
        max_table[i][j] = -max(q_table[i][j])

# Plot the surface.
fig = plt.figure()
ax = fig.gca(projection='3d')
plt.title("Q table values")
plt.xlabel("Position")
plt.ylabel("Velocity")
surf = ax.plot_surface(X, Y, max_table, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
plt.show()
