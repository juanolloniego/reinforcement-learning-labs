import numpy as np
import matplotlib.pyplot as plt

Fib_limit = 30


def fibonacci(n):
    fibonacci.calls += 1
    if n in past_fib:
        return past_fib[n]
    if n == 0 or n == 1:
        past_fib[n] = 1
        return 1
    total = fibonacci(n-1) + fibonacci(n-2)
    past_fib[n] = total
    return total


def recursive_fib(n):
    recursive_fib.calls += 1
    if n == 0 or n == 1:
        return 1
    return recursive_fib(n - 1) + recursive_fib(n - 2)


# Computer the first 30 numbers and count the number of times each function was called.
times_called = {'DP': [], 'Rec': []}

for i in range(Fib_limit + 1):
    # Delete past_fib every time so it has to recompute
    past_fib = {}
    fibonacci.calls = 0  # Used to count the number of function calls. Function attributes. Python is awesome!
    dp_result = fibonacci(i)
    times_called['DP'].append(fibonacci.calls)

    recursive_fib.calls = 0
    rec_result = recursive_fib(i)
    times_called['Rec'].append(recursive_fib.calls)


plt.title("Function calls per fibonacci number computed")
plt.plot(range(Fib_limit + 1), np.log(times_called['DP']), label='Dynamic Programming')
plt.plot(range(Fib_limit + 1), np.log(times_called['Rec']), label="Recursive")

x_marks = [i for i in range(0, Fib_limit + 1, 5)]
plt.xticks(x_marks)

plt.xlabel("Fibonacci input")
plt.ylabel("Number of function calls")
plt.legend()
plt.show()

