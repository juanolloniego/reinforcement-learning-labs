import numpy as np
import itertools as it
import matplotlib.pyplot as plt

# Hide the flag game #
number_flags = 2
number_hiding_spots = 5
number_rounds = 500
maximum_score = number_flags * number_rounds
number_iterations = 100
epsilon = 0.2
eta = 1  # Exponential distribution parameter

# Find all possible flag placements
flag_positions = list(it.combinations(range(5), 2))


def game_loop(hiding_strategy, search_strategy, seeker_history, hider_history):
    scores = []
    round_score = 0
    for current_round in range(number_rounds):

        # Hide the flags
        hiding_spots = hiding_strategy(hider_history, current_round + 1)

        # Try to find them
        search_attempts = search_strategy(seeker_history, current_round + 1)

        for flag in search_attempts:
            if flag in hiding_spots:
                round_score += 1

        seeker_history[flag_positions.index(search_attempts)] += round_score
        hider_history[flag_positions.index(hiding_spots)] -= round_score

        scores.append(round_score)
    return scores


def base_search_strategy(history, round_number):
    return fpl(history, round_number)


def uniform_random(history, round_number):
    # Parameters unused, there for consistency
    return flag_positions[np.random.choice(len(flag_positions))]


def epsilon_greedy(history, round_number):
    # Round number unused, there for consistency
    if np.random.uniform(low=0, high=1) < epsilon:
        return flag_positions[np.random.choice(len(flag_positions))]
    else:
        action_choice = np.argmax(history)
        return flag_positions[action_choice]


def fpl(history, round_number):
    exponential_noise = [np.random.exponential(scale=eta) for _ in history]
    noisy_arms = history / round_number + exponential_noise
    action = np.argmax(noisy_arms)
    return flag_positions[action]


# Initialise score and history trackers #
regrets_tracker = {'uniform': [], 'epsilon': [], 'fpl': []}
rewards_tracker = {'uniform': [], 'epsilon': [], 'fpl': []}

seeker_hist = {}
hider_hist = {}

# Define the functions and plot titles
functions = {'uniform': uniform_random, 'epsilon': epsilon_greedy, 'fpl': fpl}
titles = {'uniform': "Uniform hiding strategy",
          'epsilon': "Epsilon greedy hiding strategy with {} epsilon".format(epsilon),
          'fpl': "FPL hiding strategy"}

round_space = np.arange(1, 501)
max_round_scores = 2 * round_space

# Play the games #
for iteration in range(number_iterations):
    for key in regrets_tracker.keys():
        seeker_hist[key] = np.zeros(len(flag_positions))
        hider_hist[key] = np.zeros(len(flag_positions))

        current_scores = game_loop(functions[key], base_search_strategy, seeker_hist[key], hider_hist[key])
        regrets_tracker[key].append(max_round_scores - current_scores)
        rewards_tracker[key].append(current_scores)


# Plot the results #
for key in rewards_tracker.keys():
    rewards_tracker[key] = np.array(rewards_tracker[key]) / round_space

    print("Reward for", key, "hiding strategy:", np.average(rewards_tracker[key]))
    plt.plot(range(number_rounds), np.average(rewards_tracker[key], 0), label=key)

plt.title("Average reward per round")
plt.xlabel("Round")
plt.ylabel("Avg Reward")
plt.legend()
plt.show()

for key in regrets_tracker.keys():
    regrets_tracker[key] = np.array(regrets_tracker[key]) / round_space

    print("Regret for", key, "hiding strategy:", np.average(regrets_tracker[key]))
    plt.plot(range(number_rounds), np.average(regrets_tracker[key], 0), label=key)

plt.title("Average regret per round")
plt.xlabel("Round")
plt.ylabel("Avg Regret")
plt.legend()
plt.show()
